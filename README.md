Aplicación Movil BMW Challenge
==============================

Descripción del proyecto
------------------------
Aplicación creada para el reto BMW de Talent Land 2018. Establece una conexión Bluetooth con la Raspberry del vehiculo robot, 
por medio de la cual recibe información en tiempo real de los sensores a una frecuencia de 20 Hz. Esta información es porteriormente 
procesada en el dispositivo movil y usada para mostrar estos datos de una manera gráfica. Adicionalmente, pueden ser visualizados 
tal cual se recibe esta información en la pantalla de consola.

![](screenshot/robot_car.jpg)

Pantalla de inicio
------------------
La pantalla principal de la aplicación cuenta con los accesos a las configuraciones de Bluetooth, los distintos botones de los sensores, 
un indicador de batería del vehiculo y un boton de acceso a la pantalla de consola. 

![](screenshot/start_screen.jpg)

Conexión Bluetooth
------------------
Para establecer la conexion entre el dispositivo móvil y la Raspberry Pi, primeramente en caso de que el Bluetooth del dispositivo no este encendido, 
primero hace una petición para encenderlo. Una vez encendido, se despliega una lista de los dispositivos con los que se ha emparejado anteriormente.
Cabe destacar que antes de intentar establecer la conexión, el dispositivo móvil debe haber sido agregado a la lista de dispositivos de confianza de 
la Rasperry Pi mediante línea de comandos de Linux.

![](screenshot/request_bluetooth_enable.jpg)
![](screenshot/device_list.jpg)


Botones de los sensores
-----------------------
Los botones tienen dos formas de ser activados, ya sea con un toque corto o un toque prolongado. Con el toque corto, si el boton seleccionado 
esta inactivo, la gráfica del sensor seleccionado será mostrada y ocupará todo el espacio, por otro lado, si el boton esta activo, la gráfica 
se ocultará mostrandose en la pantalla unicamente el logotipo de BMW. En cambio si se presionan lo botones de manera prolongada, la grafíca del
sensor activado pasará a fijarse en la pantalla, dando la oportunidad de fijar hasta tres sensores al mismo tiempo.

![](screenshot/ultrasonic_sensor_chart.jpg)
![](screenshot/twice_device.jpg)
![](screenshot/screen_three_charts_together.jpg)

Indicador de batería
--------------------
El indicador de batería se muestra dependiendo del voltaje de alimentación que le llega a la Raspberry Pi. Cuando está desconectado del Bluetooth, 
se muestra una leyenda de "Unknown", pero al conectarse, comienza a recibir los datos y se muestran en porcentaje en el indicador de la aplicación. 
El ícono cambia de color dependiendo del porcentaje de la batería. Es de color azul al estar desconectado, de color verde entre 100 y 75%, de color 
amarillo entre 74 y 50%, color anaranjado entre 49 y 25% y color rojo entre 24 y 0%.

Pantalla de consola
-------------------
Se implementó tambien esta función con el fin de verificar la recepción de los datos, el formato de los mismos. En esta pantalla también es posible 
enviar datos a la Raspberry Pi, siempre y cuando exista una conexión establecida.

![](screenshot/console_screen.jpg)