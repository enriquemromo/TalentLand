package app.talentland.com.talentland.fragments.ploting;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.talentland.com.talentland.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FirstSensorFragment extends Fragment {


    public FirstSensorFragment() {
        // Required empty public constructor
    }

    public static FirstSensorFragment newInstance() {
        
        Bundle args = new Bundle();
        
        FirstSensorFragment fragment = new FirstSensorFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first_sensor, container, false);
    }

}
