package app.talentland.com.talentland;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import app.talentland.com.talentland.fragments.ploting.MainFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);

        //Es la primera vez que abre la aplicación?
        if(savedInstanceState == null){

            //1.- Obtener la instancia del Fragment a colocar
            Fragment mainFragment = MainFragment.newInstance();

            //2.- Obtener una instancia de Fragment Manager
            FragmentManager fragmentManager = this.getSupportFragmentManager();

            //3.- Iniciar una Transacción con el Fragment Manager
            FragmentTransaction transaction = fragmentManager.beginTransaction();

            //4.- Indicar/definir la transacción a realizar
            transaction.add(R.id.container,mainFragment);

            //5.- Confirmar la transacción
            transaction.commit();
        }
    }
}
