package app.talentland.com.talentland.fragments.ploting;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import app.talentland.com.talentland.R;
import app.talentland.com.talentland.fragments.bluetooth.BluetoothFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {


    private static int LAST_SENSOR_ACTIVATED = 0;
    private static int SENSOR_1 = 1;
    private static int SENSOR_2 = 2;


    public MainFragment() {
        // Required empty public constructor
    }

    public static MainFragment newInstance() {

        Bundle args = new Bundle();

        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        /// Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_main,
                container,
                false);

        return rootView;
    }

    //--- El codigo principal a partir de aqui ---


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        inflateFragmentOneByDefault();

        setFunctionalityToViewMembers(view);

    }

    private void setFunctionalityToViewMembers(View view) {

        FloatingActionButton fabBluetooth = view.findViewById(R.id.fabBluetooth);
        fabBluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Crear la nueva instancia del Bluetooth Fragment
                Fragment bluetoothFragment = BluetoothFragment.newInstance();

                //Mandamos llamar el fragmentManager pero de la actividad ya que esta vez se va a
                //sustituir el MainFragment por el bluetoothfragment y el container es el FrameLayout
                //Que esta en el activity_main
                FragmentManager fragmentManager = MainFragment.this.getActivity().getSupportFragmentManager();

                //El resto de pasos es igual, pero aqui si se agrega al backstack para poder volver al mainFragament
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, bluetoothFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });

        Button btnSensor1 = view.findViewById(R.id.btnSensor1);
        btnSensor1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(LAST_SENSOR_ACTIVATED != SENSOR_1) {
                    inflateFragmentOneByDefault();
                    Toast.makeText(v.getContext(), "FragmentOne Inflated", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button btnSensor2 = view.findViewById(R.id.btnSensor2);
        btnSensor2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(LAST_SENSOR_ACTIVATED != SENSOR_2) {
                    inflateFragmentTwo();
                    Toast.makeText(v.getContext(), "FragmentTwo Inflated", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button btnSensor3 = view.findViewById(R.id.btnSensor3);
        btnSensor3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "Sensor3 Button Clicked", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void inflateFragmentOneByDefault(){

        //Inflar el fragment del primer sensor dentro del frameLayout del mainFragment

        //Crear una nueva instancia del fragment que queremos cargar
        Fragment fragmentOne = FirstSensorFragment.newInstance();

        //Al estar en un fragment, se debe llamar al childFragmentManager
        FragmentManager fragmentManager = this.getChildFragmentManager();

        //Comenzar la transaccion como normalmente se haria
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        //Poner el fragmentOne dentro del FrameLayout que se tiene en el layout_main
        fragmentTransaction.replace(R.id.plot_container_fragment, fragmentOne);

        //Aqui no se agrega al backstack ya que solo estamos cambiando el fragment anidado dentro del mainFragment
        //fragmentTransaction.addToBackStack(null);

        //Hacer el commit
        fragmentTransaction.commit();

        LAST_SENSOR_ACTIVATED = SENSOR_1;
    }

    public void inflateFragmentTwo(){
        //Inflar el fragment del primer sensor dentro del frameLayout del mainFragment

        //Crear una nueva instancia del fragment que queremos cargar
        Fragment fragmentTwo = SecondSensorFragment.newInstance();

        //Al estar en un fragment, se debe llamar al childFragmentManager
        FragmentManager fragmentManager = this.getChildFragmentManager();

        //Comenzar la transaccion como normalmente se haria
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        //Poner el fragmentOne dentro del FrameLayout que se tiene en el layout_main
        fragmentTransaction.replace(R.id.plot_container_fragment, fragmentTwo);

        //Aqui no se agrega al backstack ya que solo estamos cambiando el fragment anidado dentro del mainFragment
        //fragmentTransaction.addToBackStack(null);

        //Hacer el commit
        fragmentTransaction.commit();

        LAST_SENSOR_ACTIVATED = SENSOR_2;
    }

}
