package app.talentland.com.talentland.fragments.ploting;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.talentland.com.talentland.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SecondSensorFragment extends Fragment {


    public SecondSensorFragment() {
        // Required empty public constructor
    }

    public static SecondSensorFragment newInstance() {
        
        Bundle args = new Bundle();
        
        SecondSensorFragment fragment = new SecondSensorFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second_sensor, container, false);
    }

}
